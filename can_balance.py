"""
Given a non-empty array, return true if there is a place to split the array so that the sum of the numbers on one side is equal to the sum of the numbers on the other side.


canBalance([1, 1, 1, 2, 1]) → true
canBalance([2, 1, 1, 2, 1]) → false
canBalance([10, 10]) → true
"""

def canBalance(nums):
    """
    Iterates through nums from beginning and end, adding the values in vars left and right, respectively
    Returns true if it has reached a 'middle' where left and right sums are equal.
    Returns False otherwise
    """
    if len(nums) < 2:
        return False
    left, right = 0, 0 #initiate the sides where we'll add the nums
    i = 0 #first index, for right to left iteration
    j = len(nums) - 1 #right to left index
    while i <= j:
        if left < right:
            left += nums[i]
            i += 1
        else:
            right += nums[j]
            j -= 1
    if left == right:
        return True
    return False

print(canBalance([10]))
