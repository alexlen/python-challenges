"""
Consider the leftmost and righmost appearances of some value in an array. We'll say that the "span" is the number of elements between the two inclusive. 
A single value has a span of 1. Returns the largest span found in the given array. (Efficiency is not a priority.)

maxSpan([1, 2, 1, 1, 3]) → 4
maxSpan([1, 4, 2, 1, 4, 1, 4]) → 6
maxSpan([1, 4, 2, 1, 4, 4, 4]) → 6
"""

def maxSpan(arr):
    if len(arr) == 0: #manage the case of an empty array
        return 0
    largestSpan = 1
    for i in range(len(arr)-1, 0, -1): #iterate through the list in reverse
        try:
            span = i + 1 - arr.index(arr[i], 0, i) # i + 1 - index of the first occurrence of the value in cause after index 0 and before index i
            if span > largestSpan: #check if we found a larger span
                largestSpan = span
        except ValueError:
            pass
        finally:
            if largestSpan > i:
                break #there is no span larger than i
    return largestSpan
    
print(maxSpan([4, 4, 4, 1, 3, 2, 2]))
