"""
Given three ints, a b c, one of them is small, one is medium and one is large. Return true if the three values are evenly spaced, so the difference between small and medium is the same as the difference between medium and large.


evenlySpaced(2, 4, 6) → true
evenlySpaced(4, 6, 2) → true
evenlySpaced(4, 6, 3) → false
"""

def evenlySpaced(a, b, c):
    """
    takes three ints and returns true if they are evenly spaced, false otherwise
    """
    asc = sorted([a, b, c])
    return asc[1] - asc[0] == asc[2] - asc[1]

print(evenlySpaced(2, 2, 0))