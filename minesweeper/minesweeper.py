"""
Implement Minesweeper
Minesweeper is a game where the objective is correctly identify the location of all mines in a given grid. You are given a uniform grid of gray squares in the beginning of the game. Each square contains either a mine (indicated by a value of 9), or an empty square. Empty squares have a number indicating the count of mines in the adjacent squares. Empty squares can have counts from zero (no adjacent mines) up to 8 (all adjacent squares are mines).

If you were to take a complete grid, for example, you can see which squares have mines and which squares are empty:
0  0  0  0  0
0  0  0  0  0
1  1  1  0  0
1  9  1  0  0
1  2  2  1  0
0  1  9  1  0
0  1  1  1  0

The squares with "2" indicate that there 2 mines adjacent to that particular square.

Gameplay starts with a user un-hiding a square at random. If the square contains a mine, the game ends. If it is a blank, the number of adjacent mines is revealed.

Exposing a zero means that there are no adjacent mines, so exposing all adjacent squares is guaranteed safe. As a convenience to the player, the game continues to expose adjacent squares until a non-zero square is reached.

For example, if you click on the top right corner you get this ('-' means hidden):
0  0  0  0  0
0  0  0  0  0
1  1  1  0  0
-  -  1  0  0
-  -  2  1  0
-  -  -  1  0
-  -  -  1  0

Please write functions to construct the playing field given the size and number of mines.
https://techdevguide.withgoogle.com/paths/foundational/coding-question-minesweeper/#!
"""

import random

class Cell:
    """
    A cell with an int value from 0 to 9
    0: cell is empty
    1 to 8: cell is empty and x of its adjacent cells are mined
    9: cell is mined
    """

    def __init__(self, value=0):
        self.value = value
        self.exposed = False

    def getValue(self):
        return self.value

    def setValue(self, value):
        self.value = value

    def isExposed(self):
        return self.exposed

    def expose(self):
        self.exposed = True

    def hide(self):
        self.exposed = False # for undos and resets

    def __str__(self):
        if self.exposed:
            return str(self.value)
        else:
            return "-"


class Edge:
    """
    A connection between two adjacent cells: from src cell to dest cell
    """

    def __init__(self, src, dest):
        self.src = src
        self.dest = dest

    def getSource(self):
        return self.src

    def getDestination(self):
        return self.dest


class Digraph:
    """
    cells: a list of cells in the graph
    edges: a dictionary mapping each cell to its adjacent cells
    """

    def __init__(self):
        self.cells = []
        self.edges = {}
    def addCell(self, cell):
        if cell in self.cells:
            raise ValueError('Duplicate cell')
        else:
            self.cells.append(cell)
            self.edges[cell] = []
    def addEdge(self, edge):
        src = edge.getSource()
        dest = edge.getDestination()
        if not(src in self.cells and dest in self.cells):
            raise ValueError('Cell not in graph')
        self.edges[src].append(dest)
    def adjacentCells(self, cell):
        return self.edges[cell]

class Graph(Digraph):
    """
    an undirected graph, where edges are added from src to dest and from dest to src
    """

    def addEdge(self, edge):
        Digraph.addEdge(self, edge)
        rev = Edge(edge.getDestination(), edge.getSource())
        Digraph.addEdge(self, rev)

class MinesweeperField(Graph):
    """
    An undirected graph with 'rows' by 'cols' cells and connections between them
    """

    def __init__(self, rows, cols, mines):
        """assumes rows, cols and mines are ints"""
        super().__init__()
        self.rows = rows
        self.cols = cols
        self.mines = mines # number of mines in the field
        self.matrix = [] # a nested list
        self.exposedCells = 0 # keep track of number of exposed cells

    def buildMatrix(self):
        """add empty cells and build a nested list"""
        for row in range(self.rows):
            self.matrix.append([])
            for col in range(self.cols):
                cell = Cell()
                super().addCell(cell)
                self.matrix[row].append(cell)

    def defineAdjacentCells(self):
        """add edges between cells"""
        # horizontal connections: edges between adjacent cells on the same row
        # xx
        for row in range(self.rows):
            for col in range(self.cols - 1):
                horizontalEdge = Edge(self.matrix[row][col], self.matrix[row][col + 1])
                self.addEdge(horizontalEdge)
        # vertical connections: edges between adjacent cells on the same column
        # x
        # x
        for col in range(self.cols):
            for row in range(self.rows - 1):
                verticalEdge = Edge(self.matrix[row][col], self.matrix[row + 1][col])
                self.addEdge(verticalEdge)
        # diagonal upper-left/bottom-right connections
        # x
        #  x
        for row in range(self.rows - 1):
            for col in range(self.cols - 1):
                diagonalEdge = Edge(self.matrix[row][col], self.matrix[row + 1][col + 1])
                self.addEdge(diagonalEdge)
        # diagonal upper-right/bottom-left connections
        #  x
        # x
        for row in range(self.rows - 1):
            for col in range(1, self.cols): # cells in first col have no bottom-left cells
                diagonalEdge = Edge(self.matrix[row][col], self.matrix[row + 1][col - 1])
                self.addEdge(diagonalEdge)
    
    def addMine(self, mine):
        """mine a cell and update adjacent cells"""
        assert mine.getValue() != 9, 'Cell is already a mine'
        mine.setValue(9)
        for cell in self.adjacentCells(mine):
            val = cell.getValue()
            if val != 9: # if adjacent cell is not a mine, increment its value
                cell.setValue(val + 1)
    
    def mineTheField(self):
        """add all the mines to the field in a random sample of cells"""
        mines = random.sample(self.cells, self.mines)
        for mine in mines:
            self.addMine(mine)
    
    def startUp(self):
        """calls all the methods needed to start playing"""
        self.buildMatrix()
        self.defineAdjacentCells()
        self.mineTheField()

    def clickCell(self, cell):
        """exposes a cell

        if cell has 0 adjacent mines it recursively calls clickCell on its adjacent cells
        if a cell is a mine, raises BoomError to end the game
        """

        assert not cell.isExposed(), 'You can already see this cell'
        cell.expose()

        val = cell.getValue()

        # if cell is a mine, game over
        if val == 9:
            raise BoomError

        # keep track of the number of exposed cells to check if game was won
        self.exposedCells += 1
        if self.exposedCells == self.rows * self.cols - self.mines:
            raise YayError

        # if cell is 0, click on all adjacent cells
        if val == 0:
            for neighbor in self.adjacentCells(cell):
                if not neighbor.isExposed():
                    self.clickCell(neighbor)

    def showSolution(self):
        """expose all cells"""
        for cell in self.cells:
            cell.expose()

    def __str__(self):
        """every cell value with spaces between columns and line breaks between rows"""
        result = ''
        for row in range(self.rows):
            for col in range(self.cols):
                result += str(self.matrix[row][col]) + ' '
            result += '\n'
        return result


class BoomError(Exception):
    """Raised when clicking on a mine to end the game"""

class YayError(Exception):
    """Raised when game is won"""

def select_level():
    """ask user to set level return a tuple with field rows, columns and number of mines"""

    # ask user to choose level
    while True:
        level = input('Type "easy", "medium" or "hard" to pick a level\n')
        if level in ['easy', 'medium', 'hard']:
            break

    if level == 'easy':
        rows = 6
        cols = 9
        mines = random.randint(5, cols) # choose between 5 and 9 mines

    if level == 'medium':
        rows = 10
        cols = 15
        mines = random.randint(cols, cols + 7) # 10-15% of cells are mines

    if level == "hard":
        rows = 16
        cols = 24
        mines = int(random.randint(15, 20) * rows * cols / 100) # 15-20% are mines

    return(rows, cols, mines)


def play_game():
    """
    generates the field and updates it as we play
    """

    rows, cols, mines = select_level()
    field = MinesweeperField(rows, cols, mines)
    field.startUp()
    print(field)
    while True:
        coords = input('Enter row and column number separated by a space to click on a cell. Enter q quit.\n').split()
        if coords[0] == 'q':
            break
        try:
            row = int(coords[0])
            col = int(coords[1])
            assert row < rows and col < cols
        except (ValueError, IndexError) as e:
            pass
        except AssertionError:
            print('These are not valid coordinates. Try again')
        else: # if we have valid coordinates
            print('\n')
            try:
                field.clickCell(field.matrix[row][col])
            except AssertionError:
                print('This cell is already shown')
            except BoomError:
                print('Game Over')
                field.showSolution()
                break
            except YayError:
                print('Congratulations! You won!')
                break
            finally:
                print(field)

def main():
    while True:
        play_game()
        if input("Play again? y/n") != "y":
            break

if __name__ == "__main__":
    main()