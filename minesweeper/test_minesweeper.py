import unittest
import minesweeper

class CellTestCase(unittest.TestCase):
    def setUp(self):
        self.cell = minesweeper.Cell()

    def test_create_empty_cell(self):
        self.assertEqual(self.cell.getValue(), 0, 'Empty cell should be 0')

    def test_is_hidden(self):
        self.assertFalse(self.cell.exposed, 'Cell should be hidden by default')

    def test_set_value(self):
        self.cell.setValue(8)
        self.assertEqual(self.cell.getValue(), 8, 'Should get the value we set')

    def test_expose_cell(self):
        self.cell.expose()
        self.assertTrue(self.cell.exposed, 'Cell should be exposed')
        self.assertEqual(str(self.cell), '0', 'Cell value should be shown after exposure')
        self.cell.hide()
        self.assertFalse(self.cell.exposed, 'Cell should be hidden again')
        self.assertEqual(str(self.cell), '-',
                         'Cell value should be hidden after exposure and hiding')

class EdgeTestCase(unittest.TestCase):
    def setUp(self):
        self.src = minesweeper.Cell()
        self.dest = minesweeper.Cell(9)
        self.edge = minesweeper.Edge(self.src, self.dest)

    def test_get_src(self):
        self.assertIs(self.edge.getSource(), self.src,
                      'getSource should return the given as first argument')

    def test_get_dest(self):
        self.assertIs(self.edge.getDestination(), self.dest,
                      'getDestination should return second arg')

class FieldTestCase(unittest.TestCase):
    def setUp(self):
        self.rows = 3
        self.cols = 4
        self.mines = 3
        self.field = minesweeper.MinesweeperField(self.rows, self.cols, self.mines)

    def test_add_cell(self):
        cell = minesweeper.Cell()
        self.field.addCell(cell)
        self.assertIs(self.field.cells[0], cell, 'addCell should append to the list of cells')

    def test_add_edge(self):
        cells = []
        for i in range(3):
            cells.append(minesweeper.Cell())
            self.field.addCell(cells[i])
        self.field.addEdge(minesweeper.Edge(cells[0], cells[1]))
        self.field.addEdge(minesweeper.Edge(cells[1], cells[2]))
        self.assertEqual(self.field.adjacentCells(cells[1]), [cells[0], cells[2]],
                         'addEdge should make 2 cells adjacent')

        outsider_cell = minesweeper.Cell()
        with self.assertRaises(ValueError):
            self.field.addEdge(minesweeper.Edge(cells[1], outsider_cell))

    def test_build_matrix(self):
        self.field.buildMatrix()
        self.assertEqual(len(self.field.cells), 12, 'buildMatrix should add 12 cells')
        self.assertEqual(len(self.field.matrix), 3, 'matrix should have 3 rows')
        self.assertEqual(len(self.field.matrix[0]), 4, 'first row should have 4 cols')

    def test_adjacent_cells(self):
        self.field.buildMatrix()
        self.field.defineAdjacentCells()
        first_cell = self.field.matrix[0][0]
        cell11 = self.field.matrix[1][1]
        last_cell = self.field.matrix[2][3]

        self.assertEqual(self.field.adjacentCells(first_cell),
                         [self.field.matrix[0][1], self.field.matrix[1][0], cell11],
                         'first cell should have 3 adjacent cells: right, bottom and bottom-right')

        self.assertEqual(self.field.adjacentCells(cell11),
                         [self.field.matrix[1][0], self.field.matrix[1][2], # left and right
                          self.field.matrix[0][1], self.field.matrix[2][1], # up and down
                          self.field.matrix[0][0], self.field.matrix[2][2], # upper left, bottom right
                          self.field.matrix[0][2], self.field.matrix[2][0]], # upper right, bottom left
                         'cell on second row second col should have these 8 neighbors')

        self.assertEqual(self.field.adjacentCells(last_cell),
                         [self.field.matrix[2][2],
                          self.field.matrix[1][3],
                          self.field.matrix[1][2]],
                         'last cell should have 3 adjacent cells: left, up and upper-left')

    def test_add_mine(self):
        """add a mine to first cell, check value and adjacent values"""

        # add two adjacent cells and the mine one
        cell0 = minesweeper.Cell()
        cell1 = minesweeper.Cell()
        self.field.addCell(cell0)
        self.field.addCell(cell1)
        self.field.addEdge(minesweeper.Edge(cell0, cell1))
        self.field.addMine(cell0)

        self.assertEqual(cell0.getValue(), 9, 'cell should have value 9')
        self.assertEqual(cell1.getValue(), 1, 'next cell should have value 1')
        with self.assertRaises(AssertionError):
            self.field.addMine(cell0) # adding a mine to a mine should raise an error

    def test_mine_the_field(self):
        """run startUp and count number of mines"""

        self.field.startUp()
        mines_found = 0
        for cell in self.field.cells:
            if cell.getValue() == 9:
                mines_found += 1

        self.assertEqual(mines_found, self.mines, 'no of mines found should be equal to given arg')

    def test_click_cell(self):
        """test all effects of click cell

        click on already exposed cell should raise AssertionError
        click on mine should raise an error to end the game
        click on cell with a value from 1 to 8 exposes the cell
        click on cell with 0 exposes the cell and calls clickCell on its adjacent cells
        """

        self.field.startUp()

        # find two adjacent cells with value 0
        for cell in self.field.cells:
            if cell.getValue() == 0:
                for neighbor in self.field.adjacentCells(cell):
                    if neighbor.getValue() == 0:
                        cell_0 = cell
                        cell_0_neighbor = neighbor
                        break
                else:
                    # we found a cell with 0 but none of its neighbors have 0, keep looking
                    continue
                break

        # click on one of the adjacent cells with 0.
        # its neighbors should become exposed.
        # it should also call clickCell cell_0_neighbor and expose its neighbors too
        self.field.clickCell(cell_0)
        for cell in [cell_0, cell_0_neighbor]:
            for neighbor in self.field.adjacentCells(cell):
                self.assertTrue(neighbor.isExposed(), 'neighbors of cells with 0 should be exposed')

        with self.assertRaises(AssertionError):
            self.field.clickCell(cell_0) # clicking on an exposed cell should raise error

        # find a hidden 'hint' cell (with value from 1 to 8) to click on
        for cell in self.field.cells:
            if 0 < cell.getValue() < 9 and not cell.isExposed():
                hint_cell = cell
                break
        self.field.clickCell(hint_cell)
        self.assertTrue(hint_cell.isExposed(), 'cell with value from 1 to 8 should be exposed')

        # find a mine to click on
        for cell in self.field.cells:
            if cell.getValue() == 9:
                mine = cell
                break
        with self.assertRaises(Exception):
            self.field.clickCell(mine) # clicking on mine should raise user defined exception


if __name__ == '__main__':
    unittest.main()
