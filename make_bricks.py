"""
We want to make a row of bricks that is goal inches long. We have a number of small bricks (1 inch each) and big bricks (5 inches each). Return True if it is possible to make the goal by choosing from the given bricks. This is a little harder than it looks and can be done without any loops. See also: Introduction to MakeBricks
https://codingbat.com/prob/p118406

make_bricks(3, 1, 8) → True
make_bricks(3, 1, 9) → False
make_bricks(3, 2, 10) → True
"""

def make_bricks(small, big, goal):
    maxBig = goal // 5 #optimal number of big bricks we can use
    minSmall = goal % 5 #the least number of small bricks we have to use 
    #we define the defficiency of big bricks as:
    #0 if we can use all the big bricks
    #(maxBig - big) if we run out of big bricks (big < maxBig)
    bigBricksDefficiency = max(0, maxBig - big)
    #if we run out of big bricks we need to complete with small bricks only:
    return bigBricksDefficiency * 5 + minSmall <= small

print(make_bricks(3, 1, 8))
