"""
Write a function that replaces the words in `raw` with the words in `code_words` such that the first occurrence of each word in `raw` is assigned the first unassigned word in `code_words`.


encoder(["a"], ["1", "2", "3", "4"]) → ["1"]
encoder(["a", "b"], ["1", "2", "3", "4"]) → ["1", "2"]
encoder(["a", "b", "a"], ["1", "2", "3", "4"]) → ["1", "2", "1"]
"""

def encoder(raw, code_words):
    """
    raw, code_words: lists of strings
    modifies 'raw' encoded with each string encoded from code_words
    """
    d = {} # a dictionary to store raw words and their assigned code words
    j = 0 # an index to control iteration through code_words
    for word in raw:
        if word not in d: # if this is the first occurence of word, pair it to the next word in code_words to it
            d[word] = code_words[j]
            j += 1
    for i in range(len(raw)): #iterate through raw again and replace every word
        raw[i] = d[raw[i]]

L1 = ["a", "b", "a"]
code_words = ["1", "2", "3", "4"]
encoder(L1, code_words)
print(L1)

