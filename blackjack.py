"""
Given 2 int values greater than 0, return whichever value is nearest to 21 without going over. Return 0 if they both go over.


blackjack(19, 21) → 21
blackjack(21, 19) → 21
blackjack(19, 22) → 19
"""

def blackjack(a, b):
    """
    returns one of a or b (ints), whichever is closer to 21 but not greater
    """
    if a <= 21:
        if b <= 21:
            return max(a, b)  # if both <=21, return the biggest
        return a # only a <= 21
    if b <= 21: # if only b <= 21
        return b
    return 0 # both go over 21

print(blackjack(22, 22))