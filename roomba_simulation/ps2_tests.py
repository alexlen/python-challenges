v#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 14:27:23 2019

@author: alex
"""

import ps2

W = 10
H = 10

origin = ps2.Position(0, 0)
room = ps2.RectangularRoom(W, H)
if room.getNumTiles() == W * H:
    print("Successfully created a room.")
    
if room.getNumCleanedTiles() == 0:
    print("All room tiles are dirty.")
    room.cleanTileAtPosition(origin)
    print("Origin tile is cleaned: ", room.isTileCleaned(0, 0) == True)
    print("Num of cleaned tiles is 1: ", room.getNumCleanedTiles() == 1)

random_pos = room.getRandomPosition()
print("Getting a random position:", random_pos)
print("Checking if this position is in room:", room.isPositionInRoom(random_pos))

print("Cleaning tile at position", random_pos, "twice")
room.cleanTileAtPosition(random_pos)
room.cleanTileAtPosition(random_pos)
print("Num of cleaned tiles is now: ", room.getNumCleanedTiles())

print("Negative position is not in room:", not room.isPositionInRoom(ps2.Position(-1,0)))